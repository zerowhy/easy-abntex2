# Easy abnTeX2
Repositório com arquivo de exemplo simplificado para abnTeX2

Esse repositório acompanha um vídeo tutorial para você aprender abnTeX, [disponível no Odysee/LBRY](https://odysee.com/@zerowhy:1/latex-abntex2-01:9).

Feito especialmente para trabalhos simples, como pesquisas escolares.

Para usar abnTeX2 no TCC ou semelhantes, veja o [repositório oficial do abnTeX2](https://github.com/abntex/abntex2)

## Setup

### Windows

Instale o [MiKTeX](https://miktex.org/download) e o [TeXstudio](https://www.texstudio.org/#download).

As dependências necessárias para compilar o documento serão instaladas automaticamente pelo MiKTeX quando for requisitado.

### Notas específicas sobre KDE

Se você estiver usando o KDE como desktop, o editor de texto [Kile](https://apps.kde.org/kile/) funciona melhor e no geral é mais agradável.
Para usá-lo, basta substituir `texstudio` por `kile` nas instruções para sua distribuição.

### Ubuntu

Para instalar o TeXStudio (editor de texto para LaTeX) e o TeXLive (com os pacotes necessários), execute esses comandos:
```bash
sudo apt-get update && sudo apt-get upgrade
sudo add-apt-repository ppa:sunderme/texstudio
sudo apt-get update
sudo apt-get install texlive-full biber texstudio
```

### Arch Linux

Para instalar o TeXStudio (editor de texto para LaTeX) e o TeXLive (com os pacotes necessários), execute esses comandos:
```bash
sudo pacman -Syu
sudo pacman -S texlive texlive-lang biber texstudio otf-latin-modern otf-latinmodern-math
```

### TeXstudio
Após abrir o TeXstudio, entre em "Opções" > "Configurar TeXstudio..." > "Compilar" e troque a "Ferramenta padrão e bibliografia" para "Biber".

Recomendo que você explore as configurações para deixar o programa da melhor maneira para você.
Para alterar as cores e estilo do app, entre em "Geral".

## Ferramentas para construção de elementos

Existem inúmeras ferramentas online para realizar diversas tarefas no LaTeX de forma gráfica.\
Aqui estão listadas algumas que eu encontrei pela web:

Fórmulas matemáticas: [Editor LaTeX Lagrida](https://latexeditor.lagrida.com/)

Gerador de tabelas: [Tables Generator](https://www.tablesgenerator.com/)

Desenhe símbolos com o mouse e receba o comando em LaTeX: [Detexify](https://detexify.kirelabs.org/classify.html)

## Recursos extras para aprendizado

PDF que você pode clicar para ver como faz várias coisas: [VisualFAQ](https://ctan.dcc.uchile.cl/info/visualfaq/visualFAQ.pdf) (muito recomendado!)\
_Nota: abra em um leitor de PDF no computador, e não no navegador. Quer um bom leitor? Baixe o [Sumatra PDF](https://www.sumatrapdfreader.org/download-free-pdf-viewer)!_

Introdução rápida em slides: [Minicurso de LaTeX](http://docente.ifrn.edu.br/diegocirilo/minicursos/apresentacao-do-minicurso-de-latex/view)

Introdução completa e extensa: [Introdução ao Uso do Preparador de Documentos LATEX](http://www.ctan.org/pkg/cursolatex)

Lista de símbolos especiais e como usá-los: [The Comprehensive LATEX Symbol List](https://linorg.usp.br/CTAN/info/symbols/comprehensive/symbols-a4.pdf)

Aprenda mais coisas: [Overleaf](https://www.overleaf.com/learn)

Resolva dúvidas: [TexFAQ](https://texfaq.org/)

Wiki abnTeX2 oficial: [wiki abntex2](https://github.com/abntex/abntex2/wiki)


Documento sobre o abnTeX2 BibLaTeX: [BibLaTeX-abnTeX2](https://raw.githubusercontent.com/abntex/biblatex-abnt/master/doc/biblatex-abnt.pdf)

Referência BibLaTeX: [BibLaTeX](https://linorg.usp.br/CTAN/macros/latex/contrib/biblatex/doc/biblatex.pdf)

## Quer contribuir?

Teve alguma ideia ou sugestão para o projeto? Por favor abra um "issue" no GitLab com sua sugestão ou se você puder abra um "Merge Request" com a mudança que você quer ver!

